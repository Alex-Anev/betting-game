import { useState } from 'react'
const style = { width: '30px', height: '30px' }

const App = () => {
  const [pcMoney, setPcMoney] = useState(100)
  const [myMoney, setMyMoney] = useState(100)
  const [pot, setPot] = useState(0)
  const [bet, placeBet] = useState(false)
  const [guess, setGuess] = useState(null)
  const [myDice, setMyDice] = useState([null, null])
  const [pcDice, setPcDice] = useState([null, null])

  const betHandler = (plusOrMin) => {
    switch (plusOrMin) {
      case 'min':
        setMyMoney(myMoney + 10)
        setPcMoney(pcMoney + 10)
        setPot(pot - 20)
        break
      case 'plus':
        setMyMoney(myMoney - 10)
        setPcMoney(pcMoney - 10)
        setPot(pot + 20)
        break
      default:
        window.alert('default betHandler')
    }
  }

  const roll = (roller) => {
    switch (roller) {
      case 0: // pc
        setPcDice([
          Math.floor((Math.random() * 6) + 1),
          Math.floor((Math.random() * 6) + 1)
        ])
        break
      case 1: // player
        setMyDice([
          Math.floor((Math.random() * 6) + 1),
          Math.floor((Math.random() * 6) + 1)
        ])
        console.log("roll player")
        break
      default:
        window.alert('default roll')
    }
  }

  const handleSetBet = () => {
    placeBet(true)
    roll(0)
  }

  function handleRollDice() {
    setMyDice(() => {
      return [
        Math.floor((Math.random() * 6) + 1),
        Math.floor((Math.random() * 6) + 1)]
    });
    if (bet === true) {

      if (myDice !== null) {
        console.log('player ', myDice)
        switch (guess) {
          case "higher": // higher
            console.log(guess, 'mda');
            console.log('pc', pcDice)
            if ((myDice[0] + myDice[1]) > (pcDice[0] + pcDice[1])) {
              console.log("Guessed higher. You win")
            } else if ((myDice[0] + myDice[1]) < (pcDice[0] + pcDice[1])) {
              console.log("Guessed higher. You lose")
            } else { console.log("higher else") }
            break
          case "lower":
            if ((myDice[0] + myDice[1]) > (pcDice[0] + pcDice[1])) {
              console.log("Guessed lower. You lose")
            } else if ((myDice[0] + myDice[1]) < (pcDice[0] + pcDice[1])) {
              console.log("Guessed lower. You win")
            } else { console.log("lower else") }
            break
          default:
            console.log("default myDice !== null")
        }
        // placeBet(false)
      } else { console.log("mydice is null") }
    } else {
      window.alert('bet is false handleRollDice')
    }
  }

  return (
    <>
      <p>Pc Money: {pcMoney}лв</p>
      <p>My Money: {myMoney}лв</p>
      <p>Pot: {pot}лв</p>
      <button style={style} onClick={() => betHandler('plus')}>+</button>
      <button style={style} onClick={() => betHandler('min')}>-</button>
      <button style={{ height: '30px' }} onClick={() => handleSetBet()}>Place Bet</button>
      <p>Pc Dice: {pcDice !== [null, null] ? pcDice[0] + ' + ' + pcDice[1] + ' = ' + (pcDice[0] + pcDice[1]) : 'no throws yet'}</p>
      <p>My Dice: {myDice !== [null, null] ? myDice[0] + ' + ' + myDice[1] + ' = ' + (myDice[0] + myDice[1]) : 'no throws yet'}</p>
      <button style={style} onClick={() => setGuess('higher')}>↑</button>
      <button style={style} onClick={() => setGuess('lower')}>↓</button>
      <button
        style={{ height: '30px' }}
        onClick={handleRollDice}
      >Roll Dice</button>
    </>
  );
}

export default App;
